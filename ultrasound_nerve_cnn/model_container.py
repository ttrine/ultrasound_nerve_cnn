import os
import cv2
import numpy as np

from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras import backend as K

from ultrasound_nerve_cnn import dataset

smooth = 1.

def dice_coef(y_true, y_pred):
	y_true_f = K.flatten(y_true)
	y_pred_f = K.flatten(y_pred)
	intersection = K.sum(y_true_f * y_pred_f)
	return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

def dice_coef_loss(y_true, y_pred):
	return -dice_coef(y_true, y_pred)

class ModelContainer:
	def __init__(self,name,model,preprocess, optimizer=Adam(lr=1e-5)):
		self.name = name
		self.preprocess = preprocess

		model.compile(optimizer=optimizer, loss=dice_coef_loss, metrics=[dice_coef])
		self.model = model

		train_images, train_masks = dataset.load_train_data()
		test_images, test_ids = dataset.load_test_data()
		
		train_images = preprocess(train_images).astype('float32')
		train_masks = preprocess(train_masks, image_flag=False).astype('float32')
		test_images = preprocess(test_images).astype('float32')

		mean = np.mean(train_images[0])
		std = np.std(train_images[0])

		train_images[0] -= mean
		train_images[0] /= std
		train_masks /= 255.
		
		test_images -= mean
		test_images /= std

		self.train_images = train_images
		self.train_masks = train_masks
		self.test_images = test_images
		self.test_ids = test_ids

		self.image_cols = train_images.shape[3]
		self.image_rows = train_images.shape[2]

	''' Trains the model according to the desired 
		specifications. '''
	def train(self, weight_file=None, nb_epoch=40, batch_size=32):
		model_folder = '../data/models/' + self.name + '/'
		if not os.path.exists(model_folder):
			os.makedirs(model_folder)

		if weight_file is not None:
			self.model.load_weights(model_folder+self.name+weight_file)

		model_checkpoint = ModelCheckpoint(model_folder+self.name+'_{epoch:02d}-{loss:.2f}.hdf5', monitor='loss', save_best_only=True)

		self.model.fit(self.train_images, self.train_masks, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, shuffle=True,
				callbacks=[model_checkpoint])

	''' Runs the test set through the network and 
		converts the result to regulation format. '''
	def evaluate(self, weight_file, submission_name=None):
		if submission_name is None: submission_name = weight_file.split('.hdf5')[0] + '_submission'
		model_folder = '../data/models/' + self.name + '/'

		def prep(img):
			img = img.astype('float32')
			img = cv2.threshold(img, 0.5, 1., cv2.THRESH_BINARY)[1].astype(np.uint8)
			img = cv2.resize(img, (self.image_cols, self.image_rows))
			return img

		def run_length_enc(label):
			from itertools import chain
			x = label.transpose().flatten()
			y = np.where(x > 0)[0]
			if len(y) < 10:
				return ''
			z = np.where(np.diff(y) > 1)[0]
			start = np.insert(y[z+1], 0, y[0])
			end = np.append(y[z], y[-1])
			length = end - start
			res = [[s+1, l+1] for s, l in zip(list(start), list(length))]
			res = list(chain.from_iterable(res))
			return ' '.join([str(r) for r in res])

		self.model.load_weights(model_folder+weight_file)

		test_images = self.model.predict(self.test_images, verbose=1)

		argsort = np.argsort(self.test_ids)
		test_ids = self.test_ids[argsort]
		test_images = test_images[argsort]

		# Convert to RLE
		total = test_images.shape[0]
		ids = []
		rles = []
		for i in range(total):
			img = test_images[i, 0]
			img = prep(img)
			rle = run_length_enc(img)

			rles.append(rle)
			ids.append(test_ids[i])

			if i % 100 == 0:
				print('{}/{}'.format(i, total))

		# Write to CSV
		with open('../data/models/'+self.name+'/'+submission_name+'.csv', 'w+') as f:
			f.write('img,pixels\n')
			for i in range(total):
				s = str(ids[i]) + ',' + rles[i]
				f.write(s + '\n')
