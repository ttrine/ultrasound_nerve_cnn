import numpy as np
import cv2

from keras.models import Model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D, Dropout
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import ZeroPadding2D

from ultrasound_nerve_cnn.model_container import ModelContainer

image_rows = 64
image_cols = 80

def preprocess(images,image_flag=False):
	# Initialize a new tensor
	preprocessed_images = np.ndarray((images.shape[0], images.shape[1], image_rows, image_cols), dtype=np.uint8)
	
	# Resize each image slice
	for i in range(images.shape[0]):
		preprocessed_images[i, 0] = cv2.resize(images[i, 0], (image_cols, image_rows), interpolation=cv2.INTER_CUBIC)
		if image_flag: # Image-specific preprocessing
			pass
	return preprocessed_images

	if image_flag: # Image-only preprocessing
		pass

def construct():
	inputs = Input((1, image_rows, image_cols))
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
	conv1 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv1)
	# conv1 = ZeroPadding2D(padding=(1, 1))(conv1)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	conv1 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv1)
	# conv1 = ZeroPadding2D(padding=(1, 1))(conv1)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	conv1 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv1)
	# conv1 = ZeroPadding2D(padding=(1, 1))(conv1)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	conv1 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv1)
	# conv1 = ZeroPadding2D(padding=(1, 1))(conv1)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	conv1 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv1)
	# conv1 = ZeroPadding2D(padding=(1, 1))(conv1)
	pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
	
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
	conv2 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv2)
	# conv2 = ZeroPadding2D(padding=(1, 1))(conv2)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	conv2 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv2)
	# conv2 = ZeroPadding2D(padding=(1, 1))(conv2)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	conv2 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv2)
	# conv2 = ZeroPadding2D(padding=(1, 1))(conv2)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	conv2 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv2)
	# conv2 = ZeroPadding2D(padding=(1, 1))(conv2)
	pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
	
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
	conv3 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv3)
	# conv3 = ZeroPadding2D(padding=(1, 1))(conv3)
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
	conv3 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv3)
	# conv3 = ZeroPadding2D(padding=(1, 1))(conv3)
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
	conv3 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv3)
	# conv3 = ZeroPadding2D(padding=(1, 1))(conv3)
	pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
	pool3 = Dropout(.5)(pool3)
	
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
	conv4 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv4)
	# conv4 = ZeroPadding2D(padding=(1, 1))(conv4)
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
	conv4 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv4)
	# conv4 = ZeroPadding2D(padding=(1, 1))(conv4)
	pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
	pool4 = Dropout(.5)(pool4)
	
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
	conv5 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv5)
	# conv5 = ZeroPadding2D(padding=(1, 1))(conv5)
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)
	conv5 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv5)
	# conv5 = ZeroPadding2D(padding=(1, 1))(conv5)
	conv5 = Dropout(.5)(conv5)
	
	up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
	conv6 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv6)
	# conv6 = ZeroPadding2D(padding=(1, 1))(conv6)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)
	conv6 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv6)
	# conv6 = ZeroPadding2D(padding=(1, 1))(conv6)
	conv6 = Dropout(.5)(conv6)
	
	up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
	conv7 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv7)
	# conv7 = ZeroPadding2D(padding=(1, 1))(conv7)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)
	conv7 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv7)
	# conv7 = ZeroPadding2D(padding=(1, 1))(conv7)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)
	conv7 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv7)
	# conv7 = ZeroPadding2D(padding=(1, 1))(conv7)
	
	up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
	conv8 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv8)
	# conv8 = ZeroPadding2D(padding=(1, 1))(conv8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)
	conv8 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv8)
	# conv8 = ZeroPadding2D(padding=(1, 1))(conv8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)
	conv8 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv8)
	# conv8 = ZeroPadding2D(padding=(1, 1))(conv8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)
	conv8 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv8)
	# conv8 = ZeroPadding2D(padding=(1, 1))(conv8)
	
	up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
	conv9 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv9)
	# conv9 = ZeroPadding2D(padding=(1, 1))(conv9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
	conv9 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv9)
	# conv9 = ZeroPadding2D(padding=(1, 1))(conv9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
	conv9 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv9)
	# conv9 = ZeroPadding2D(padding=(1, 1))(conv9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
	conv9 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv9)
	# conv9 = ZeroPadding2D(padding=(1, 1))(conv9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
	conv9 = BatchNormalization(epsilon=1e-06, mode=1, axis=-1, momentum=0.9, weights=None, beta_init='zero', gamma_init='one')(conv9)
	# conv9 = ZeroPadding2D(padding=(1, 1))(conv9)
	
	conv10 = Convolution2D(1, 1, 1, activation='sigmoid')(conv9)
	
	model = Model(input=inputs, output=conv10)
	
	return model

if __name__ == '__main__':
	m = ModelContainer('tits',construct(),preprocess)
	m.train()
	
