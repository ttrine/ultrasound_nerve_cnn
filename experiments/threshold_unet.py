import numpy as np
import cv2

from keras.models import Model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D, Dropout

from ultrasound_nerve_cnn.model_container import ModelContainer
from experiments.unet import image_rows, image_cols

def preprocess(images,image_flag=True):
	# Initialize a new tensor
	if image_flag:
		preprocessed = np.ndarray((images.shape[0], 4, image_rows, image_cols), dtype=np.uint8)
	else: 
		preprocessed = np.ndarray((images.shape[0], 1, image_rows, image_cols), dtype=np.uint8)
	
	# Resize each image slice
	for i in range(images.shape[0]):
		preprocessed[i, 0] = cv2.resize(images[i, 0], (image_cols, image_rows), interpolation=cv2.INTER_CUBIC)
		if image_flag: # Image-specific preprocessing
			preprocessed[i, 1] = cv2.threshold(preprocessed[i,0], 64, 1., cv2.THRESH_BINARY)[1]
			preprocessed[i, 2] = cv2.threshold(preprocessed[i,0], 127, 1., cv2.THRESH_BINARY)[1]
			preprocessed[i, 3] = cv2.threshold(preprocessed[i,0], 191, 1., cv2.THRESH_BINARY)[1]
	return preprocessed

def construct():
	inputs = Input((4, image_rows, image_cols))
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
	
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
	
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
	pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
	
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
	pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
	
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)
	
	up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)
	
	up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)
	
	up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)
	
	up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
	
	conv9 = Dropout(.5)(conv9)
	
	conv10 = Convolution2D(1, 1, 1, activation='sigmoid')(conv9)
	
	model = Model(input=inputs, output=conv10)
	
	return model

if __name__ == '__main__':
	m = ModelContainer('threshold_unet',construct(),preprocess)
	m.train()
	m.evaluate('threshold_unet_39.hdf5')
